# haoyundada-multi-app

用于haoyundada的多应用支持

## 安装

~~~
composer require haoyundada/haoyundada-multi-app
~~~

## 使用

用法参考开发手册[多应用模式](https://www.haoyundada.com)章节。

