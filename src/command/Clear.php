<?php
// +----------------------------------------------------------------------
// | HaoyundadaWordpress [ WE CAN DO IT JUST HAOYUNDADA IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind        : 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
namespace haoyundada\app\command;

use haoyundada\console\Command;
use haoyundada\console\Input;
use haoyundada\console\input\Argument;
use haoyundada\console\input\Option;
use haoyundada\console\Output;

class Clear extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('clear')
            ->addArgument('app', Argument::OPTIONAL, 'app name .')
            ->addOption('cache', 'c', Option::VALUE_NONE, 'clear cache file')
            ->addOption('log', 'l', Option::VALUE_NONE, 'clear log file')
            ->addOption('dir', 'r', Option::VALUE_NONE, 'clear empty dir')
            ->setDescription('Clear runtime file');
    }

    protected function execute(Input $input, Output $output)
    {
        $app         = $input->getArgument('app') ?: '';
        $runtimePath = $this->app->getRootPath() . 'runtime' . DIRECTORY_SEPARATOR . ($app ? $app . DIRECTORY_SEPARATOR : '');

        if ($input->getOption('cache')) {
            $path = $runtimePath . 'cache';
        } elseif ($input->getOption('log')) {
            $path = $runtimePath . 'log';
        } else {
            $path = $runtimePath;
        }

        $rmdir = $input->getOption('dir') ? true : false;
        $this->clear(rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR, $rmdir);

        $output->writeln("<info>Clear Successed</info>");
    }

    protected function clear(string $path, bool $rmdir): void
    {
        $files = is_dir($path) ? scandir($path) : [];

        foreach ($files as $file) {
            if ('.' != $file && '..' != $file && is_dir($path . $file)) {
                array_map('unlink', glob($path . $file . DIRECTORY_SEPARATOR . '*.*'));
                if ($rmdir) {
                    rmdir($path . $file);
                }
            } elseif ('.gitignore' != $file && is_file($path . $file)) {
                unlink($path . $file);
            }
        }
    }
}
